package com.example.log4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class AppMethod {
    Logger logger = LogManager.getLogger(getClass());

    public String sayHello() {
        logger.info("Hello World INFO");
        logger.error("Hello World ERROR");
        return "Hello World!";
    }
}
