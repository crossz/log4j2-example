package com.example.log4j;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class OtherFramework {
	
	private static final Logger UNKNOWN_ERROR_LOGGER = LoggerFactory.getLogger(OtherFramework.class);
	
	public static void throwException() {
		UNKNOWN_ERROR_LOGGER.error("", new RuntimeException("Exception From Other Framework"));
	}

}
