package com.example.log4j;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Log4j2Tracestack {
	private static final Logger logger = LogManager.getLogger(Log4j2Tracestack.class.getName());

	public static void main(String[] args) {
		Log4j2Tracestack log4j2Tracestack = new Log4j2Tracestack();
        log4j2Tracestack.stack();
	}


	public void stack() {
        logger.info("This is INFO message!");
        logger.warn("This is WARN message!");
        logger.error("This is ERROR message!");

        try {
//            this.testClassNotFoundEx(); // exception 1
            this.testNullPointerEx(); // exception 2
        } catch (ClassNotFoundException ce) {
            util_loggingKnowEx(ce);
        } catch (Exception e) {
            util_loggingUnknowEx(e);
        }

    }


    public void util_loggingKnowEx(Exception e){
        logger.info("INFO - " + e.getMessage() + " -- " + e.getCause());
        logger.warn("WARN - " + e.getMessage() + " -- " + e.getCause());
    }

    public void util_loggingUnknowEx(Exception e){
        logger.info("INFO - " + e.getMessage() + " -- " + e.getCause(), e);
        logger.error("ERROR - " + e.getMessage() + " -- " + e.getCause(), e);
    }


    public void testClassNotFoundEx() throws Exception {
        try {
            throw new ClassNotFoundException("test1 - The Caused Exception");
        } catch (Exception e) {
            // e.printStackTrace();
            throw new ClassNotFoundException("test1 - The Caught Exception", e);
        }
    }

    public void testNullPointerEx() throws Exception {
        try {
            throw new NullPointerException("test2 - The Caused Exception");
        } catch (Exception e) {
            // e.printStackTrace();
            throw new RuntimeException("test2 - The Caught Exception", e);
        }
    }


}
