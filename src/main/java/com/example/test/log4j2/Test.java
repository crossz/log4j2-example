package com.example.test.log4j2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.example.log4j.OtherFramework;

public class Test {

	private static final Logger LOGGER = LoggerFactory.getLogger(Test.class);
	
	private static final Logger UNKNOWN_ERROR_LOGGER = LoggerFactory.getLogger("UNKNOWN_ERROR");
	
	public static void main(String[] args) {
		LOGGER.info("Program Start");
		
		scenarioOne();
		
		scenarioTwo();

		scenarioThree();
		
		scenarioFour();
		
		LOGGER.info("Program Shutdown");
	}
	
	/**
	 * catch known exception (not serious)
	 */
	public static void scenarioOne() {
		try {
			LOGGER.info("Normal Program Info");

			throwKnownException();
		} catch (NullPointerException e) {
			LOGGER.warn("Catch Known NPE (not serious)");
		} catch (Exception e) {
			UNKNOWN_ERROR_LOGGER.error("", e);
		}
	}
	
	/**
	 * catch known exception (serious)
	 */
	public static void scenarioTwo() {
		try {
			LOGGER.info("Normal Program Info");

			throwKnownException();
		} catch (NullPointerException e) {
			LOGGER.error("Catch Known NPE (serious)");
		} catch (Exception e) {
			UNKNOWN_ERROR_LOGGER.error("", e);
		}
	}
	
	/**
	 * catch unknown exception
	 */
	public static void scenarioThree() {
		try {
			LOGGER.info("Normal Program Info");

			throwUnknownException();
		} catch (NullPointerException e) {
			LOGGER.warn("Catch Known NPE", e);
		} catch (Exception e) {
			UNKNOWN_ERROR_LOGGER.error("", e);
		}
	}
	
	/**
	 * Uncaught Framwork Exception
	 */
	public static void scenarioFour() {
		OtherFramework.throwException();
	}
	
	public static void throwKnownException() {
		throw new NullPointerException("Known NPE");
	}
	
	public static void throwUnknownException() {
		throw new RuntimeException("Unknown Exception");
	}
	
	
}
