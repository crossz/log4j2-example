# log4j2 Examples for Log monitoring Preparing

1. Known Errors:
   
    - info.log: one row message;
    - warn.log: one row message;
    - error.log: no printing;
    - console: everything;


2. Unknown Errors:

    - info.log: trace stack;
    - warn.log: no printing or whatever;
    - error.log: trace stack;
    - console: everything;


Notes: 
    
    - no trace printing needed for known errors.
    - console: print everything for whole scenario recording;
    - warn.log: for human reading, not for log monitoring or alerts.
